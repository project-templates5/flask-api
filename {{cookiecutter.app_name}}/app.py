from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello():
    return "Welcome to {{ cookiecutter.project_name }}"

@app.route('/app')
def app_name():
    return "App Name: {{ cookiecutter.app_name }}"

@app.route('/description')
def app_name():
    return "Project Description: {{ cookiecutter.project_short_description }}"

if __name__ == '__main__':
    app.run(host="0.0.0.0", port={{ cookiecutter.listen_port }})